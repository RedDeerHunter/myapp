﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using System.Text;

namespace MyApp.ViewModel
{
    class RegisterViewModel : ViewModelBase
    {

        public event PropertyChangedEventHandler PropertyChanged;

        
        private string _firstName;
        private string _lastName;
        private string _mypassword;
        private string _myemail;
        public String MyEmail
        {
            get { return _myemail; }
            set { SetProperty<string>(ref _myemail, value); }
        }
    }
}
