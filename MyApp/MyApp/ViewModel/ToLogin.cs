﻿using MyApp.View;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace MyApp.ViewModel
{
    class ToLogin: ContentPage
    {
        private MainPage mainPage;

        public ToLogin(MainPage mainPage)
        {
            this.mainPage = mainPage;
            NextCommand = new Command(MaCommand);
            NextRegister = new Command(MaRegister);
        }

        public ICommand NextCommand { get; private set; }

        async void MaCommand()
        {
            App.Current.MainPage = new LoginView();
        }

        public ICommand NextRegister { get; private set; }

        async void MaRegister()
        {
            App.Current.MainPage = new RegisterView();
        }

    }
}
