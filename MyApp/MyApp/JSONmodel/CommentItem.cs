﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyApp.JSONmodel
{
    public class CommentItem
    {
        [JsonProperty("date")]
        public DateTime Date { get; set; }

        [JsonProperty("author")]
        public UserItem Author { get; set; }

        [JsonProperty("text")]
        public string Text { get; set; }
    }
}
