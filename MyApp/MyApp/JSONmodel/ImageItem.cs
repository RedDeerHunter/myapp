﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyApp.JSONmodel
{
    public class ImageItem
    {
        [JsonProperty("id")]
        public int Id { get; set; }
    }
}
