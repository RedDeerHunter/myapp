﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyApp.JSONmodel
{
    public class CreateCommentRequest
    {
        [JsonProperty("text")]
        public string Text { get; set; }
    }

}
